<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    Ini adalah kode OTP Anda : {{ $otp_code->otp_code }}, Kode OTP ini hanya berlaku 5 Menit. Jangan berikan kode ini kepada siapapun, termasuk ke tim laravel.
</body>
</html>
