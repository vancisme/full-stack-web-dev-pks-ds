<?php

namespace App\Listeners;

use App\Events\UserStoredEvent;
use App\Mail\UserOTPMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailOTPCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserStoredEvent  $event
     * @return void
     */
    public function handle(UserStoredEvent $event)
    {
        dd($event);
        Mail::to($event->otp_code->user->email)->send(new UserOTPMail($event->otp_code->otp_code));
    }
}
