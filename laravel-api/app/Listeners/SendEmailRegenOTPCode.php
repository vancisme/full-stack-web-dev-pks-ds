<?php

namespace App\Listeners;

use App\Mail\UserRegenOTPCode;
use App\Events\UserStoredEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailRegenOTPCode
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserStoredEvent  $event
     * @return void
     */
    public function handle(UserStoredEvent $event)
    {
        Mail::to($event->otp_code->user->email)->send(new UserRegenOTPCode($event->otp_code->otp_code));
    }
}
