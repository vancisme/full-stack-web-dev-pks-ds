<?php

namespace App\Http\Controllers;


use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index()
    {
            $roles = Role::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Get All Role',
            'data'    => $roles
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Role::create([
            'name'  => $request->name
        ]);

        if ($role) {

            return response()->json([
                'success' => true,
                'message' => 'Role is added successfully',
                'data'    => $role
            ], 201);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role failed to save',
        ], 409);
    }

    public function show(Role $role)
    {
        $roles = Role::findOrfail($role);

        return response()->json([
            'success' => true,
            'message' => 'Get Detail Role',
            'data'    => $roles
        ], 200);
    }

    public function update(Request $request, Role $role)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $roles = Role::findOrFail($role->id);

        if ($roles) {

            $roles->update([
                'name'     => $request->name
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role is update successfully',
                'data'    => $roles
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role not found',
        ], 404);
    }

    public function destroy(Role $role)
    {
        $roles = Role::findOrfail($role->id);

        if ($roles) {

            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role is delete successfully',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role not found',
        ], 404);
    }
}
