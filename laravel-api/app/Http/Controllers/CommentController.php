<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentStoredEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{    
    
    @return void
    
    public function index()
    {
        //get data from table posts
        $comments = Comment::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data'    => $comments  
        ], 200);

    }
    
    public function show($id)
    {
        //find post by ID
        $comment = Comment::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data'    => $comment 
        ], 200);

    }
    
    
    public function store(Request $request)
    {
        //validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comment = Comment::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id
        ]);

        //memanggil event CommentStoreEvent
        event(new CommentStoredEvent($comment));


        //success save to database
        if($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Post Created',
                'data'    => $comment  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save',
        ], 409);

    }
    
    public function update(Request $request, Comment $comment)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comment = Comment::findOrFail($comment->id);

        if($comment) {
            $user = auth()->user();

            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik anda',
                ], 403);
            }
            //update comment
            $comment->update([
                'content'     => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $comment  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);

    }
    
    public function destroy($id)
    {
        //find post by ID
        $comment = Comment::findOrfail($id);

        if($comment) {
            $user = auth()->user();

            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik anda',
                ], 403);
            }

            //delete comment
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }
}
