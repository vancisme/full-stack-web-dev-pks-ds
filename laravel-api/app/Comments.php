<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table ='comments';
    protected $fillable = ['content', 'post_id', 'users_id'];
    protected $primarykey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected static function boot()
    {
        parent::boot();
        
        static::creating(function($model){
            if(empty($model->{$model->getKeyName()}))
            {
                $model->{$model->getKeyName()} = Str::uuid();
            }
            $comment->user_id = auth()->user()->id;
        });
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
