// jawaban  No 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
daftarHewan.forEach(function (item) {
console.log(item);
});


// jawaban  No 2

function introduce(data) {
    return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}!`;
}

var data = {
    name: "John",
    age: 30,
    address: "Jalan Pelesiran",
    hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan);


// jawaban No 3

function hitung_huruf_vokal(str) {
    const count = str.match(/[aeiou]/gi).length;
    return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2);


// jawaban No 4

function hitung(numbers) {
    return 2 + (numbers - 2) * 2;
}
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
