// Jawaban  No 1

const persegipanjang = (panjang, lebar) => {
    console.log("Hasil luas persegi panjang adalah : " + panjang * lebar);
    console.log("Hasil keliling persegi panjang adalah: " + 2 * (panjang + lebar));
};

persegipanjang(15, 8);



//Jawwaban No 2

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        
        fullName() {
            console.log(firstName + " " + lastName);
        },
    };
};

newFunction("William", "Imoh").fullName();



//Jawaban No 3

const person = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "Playing Football",
};
const { firstName, lastName, address, hobby } = person;
console.log(firstName, lastName, address, hobby);


//Jawaban No 4

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

console.log(combined);


//Jawaban  No 5

const planet = "earth";
const view = "glass";
console.log(
    `Lorem ${view} dolor sit amet, consectetur adipiscing alit, ${planet}`
);
