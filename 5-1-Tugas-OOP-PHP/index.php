<?php


abstract class Hewan
{
    public $nama;
    public $jumlahKaki;
    public $keahlian;
    public $darah = 50;
    
    public function atraksi(){
        echo "Atraksi : $this->nama sedang $this->keahlian";
    }
}

abstract class Fight extends Hewan
{
    public $attackPower;
    public $defencePower;
    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->darah;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    
    abstract public function intro();
    public function serang($nama){
        return "$this->nama Menyerang $nama";
    }

    public function diserang($attack){
        return "$this->nama Sedang diserang Sisa Darah :(".  ($this->darah - $attack). ")";
    }
}

class Harimau extends Fight {
    public function intro()
    {
    
    }

    public function getInfoHewan()
    {
        return "Jenis Hewan : $this->nama <br> Jumlah kaki : $this->jumlahKaki <br> Keahlian : $this->keahlian <br> attackPower : $this->attackPower <br> defencePower : $this->defencePower <br> Darah : $this->darah";
    }

}

class Elang extends Fight {

    public function intro(){
    
    }
    public function getInfoHewan()
    {
        return "Jenis Hewan : $this->nama <br> Jumlah kaki : $this->jumlahKaki <br> Keahlian : $this->keahlian <br> attackPower : $this->attackPower <br> defencePower : $this->defencePower <br> Darah : $this->darah";
    }
}
$harimau = new Harimau('harimau_1', 4, 'lari cepat', 7, 8);
echo '<h4> Method getInfoHewan</h4>';
echo $harimau->getInfoHewan();
echo '<h4> Method atraksi</h4>';
echo $harimau->atraksi();


$elang = new Elang('elang_3', 2, 'terbang tinggi', 10, 5);
echo '<h4> Method getInfoHewan</h4>';
echo $elang->getInfoHewan();
echo '<h4> Method atraksi</h4>';
echo $elang->atraksi();
echo '<h4> Method Serang : Harimau</h4>';
echo $harimau->serang($elang->nama);
echo '<h4> Method diserang : Elang</h4>';
if($harimau->serang($elang->nama)){
    echo $elang->diserang($harimau->attackPower);
}   

echo '<h4> Method Serang : Elang</h4>';
echo $elang->serang($harimau->nama);
echo '<h4> Method diserang : Harimau</h4>';
if($elang->serang($harimau->nama)){
    echo $harimau->diserang($elang->attackPower);
}   

?>
